#include "VolumetricFog/volumetricFog.h"
#include "Materials\Brouillard\brouillard.h"

volumetricFog::volumetricFog(int x, int y, int z)
{
	this->x = x;
	this->y = y;
	this->z = z;
	fogAbsorbance = new glm::vec4**[x];
	for (int i = 0; i < x; i++) {
		this->fogAbsorbance[i] = new glm::vec4*[y];
		for (int j = 0; j < y; j++) {
			this->fogAbsorbance[i][j] = new glm::vec4[z];
			for (int l = 0; l < z; l++) {
				this->fogAbsorbance[i][j][l] = glm::vec4(0);
			}
		}
	}
}

void volumetricFog::preRender(std::vector<Node*> fogs)
{
	for (int l = 0; l < fogs.size(); l++) {
		glm::mat4 inverseModel = glm::inverse(*(fogs[l]->frame()->getMatrix()));
		for (int i = 0; i < x; i++) {
			for (int j = 0; j < y; j++) {
				for (int k = 0; k < z; k++) {
					glm::vec4 pos = glm::vec4((float)i / x, (float)j / y, (float)k / z, 1);
					glm::vec4 modelpos = inverseModel * pos;
					Brouillard* b = (Brouillard*)fogs[l]->getMaterial();
					fogAbsorbance[i][j][k] += b->getOpacity(modelpos);
				}
			}
		}
	}


}

volumetricFog::~volumetricFog()
{

}