#include "voxel.h"


voxel::voxel(int largeur, int hauteur, int profondeur)
{
	for (int i = 0; i < largeur; i++) {
		for (int j = 0; j < hauteur; j++) {
			for (int k = 0; k < profondeur; k++) {
				listVertex.push_back(glm::vec3(i, j, k));
			}
		}
	}

	//creation des face du voxel sur le plan zy;
	bool flip = false;
	for (int i = 0; i < largeur; i++) {
		for (int j = 0; j < hauteur-1; j++) {
			for (int k = 0; k < profondeur-1; k++) {
				Face face1;
				Face face2;
				if (flip) {
					face1.s1 = i*hauteur*profondeur + j*profondeur + k;
					face1.s2 = i*hauteur*profondeur + j*profondeur + k +1;
					face1.s3 = i*hauteur*profondeur + (j+1)*profondeur + k;
					
					face2.s1 = i*hauteur*profondeur + (j + 1)*profondeur + k;
					face2.s2 = i*hauteur*profondeur + (j + 1)*profondeur + k + 1;
					face2.s3 = i*hauteur*profondeur + j*profondeur + k + 1;
						
				}
				else {
					face1.s1 = i*hauteur*profondeur + j*profondeur + k;
					face1.s2 = i*hauteur*profondeur + (j + 1)*profondeur + k;
					face1.s3 = i*hauteur*profondeur + (j + 1)*profondeur + k + 1;
					
					face2.s1 = i*hauteur*profondeur + j*profondeur + k;
					face2.s2 = i*hauteur*profondeur + j*profondeur + k + 1;
					face2.s3 = i*hauteur*profondeur + (j + 1)*profondeur + k + 1;
				}
				flip = !flip;
			}
		}
	}
	//creation des face du voxel sur le plan zx;
	bool flip = false;
	for (int i = 0; i < largeur - 1; i++) {
		for (int j = 0; j < hauteur; j++) {
			for (int k = 0; k < profondeur - 1; k++) {
				Face face1;
				Face face2;
				if (flip) {
					face1.s1 = i*hauteur*profondeur + j*profondeur + k;
					face1.s2 = i*hauteur*profondeur + j*profondeur + k + 1;
					face1.s3 = (i+1)*hauteur*profondeur + j*profondeur + k;

					face2.s1 = (i+1)*hauteur*profondeur + j*profondeur + k;
					face2.s2 = (i+1)*hauteur*profondeur + j*profondeur + k + 1;
					face2.s3 = i*hauteur*profondeur + j*profondeur + k + 1;

				}
				else {
					face1.s1 = i*hauteur*profondeur + j*profondeur + k;
					face1.s2 = (i+1)*hauteur*profondeur + j*profondeur + k;
					face1.s3 = (i+1)*hauteur*profondeur + j*profondeur + k + 1;

					face2.s1 = i*hauteur*profondeur + j*profondeur + k;
					face2.s2 = i*hauteur*profondeur + j*profondeur + k + 1;
					face2.s3 = (i+1)*hauteur*profondeur + j*profondeur + k + 1;
				}
				flip = !flip;
			}
		}
	}
	//creation des face du voxel sur le plan zy;
	bool flip = false;
	for (int i = 0; i < largeur - 1; i++) {
		for (int j = 0; j < hauteur - 1; j++) {
			for (int k = 0; k < profondeur; k++) {
				Face face1;
				Face face2;
				if (flip) {
					face1.s1 = i*hauteur*profondeur + j*profondeur + k;
					face1.s2 = i*hauteur*profondeur + (j+1)*profondeur + k;
					face1.s3 = (i + 1)*hauteur*profondeur + j*profondeur + k;

					face2.s1 = (i + 1)*hauteur*profondeur + j*profondeur + k;
					face2.s2 = (i + 1)*hauteur*profondeur + (j + 1)*profondeur + k;
					face2.s3 = i*hauteur*profondeur + (j + 1)*profondeur + k;

				}
				else {
					face1.s1 = i*hauteur*profondeur + j*profondeur + k;
					face1.s2 = (i + 1)*hauteur*profondeur + j*profondeur + k;
					face1.s3 = (i + 1)*hauteur*profondeur + (j + 1)*profondeur + k;

					face2.s1 = i*hauteur*profondeur + j*profondeur + k;
					face2.s2 = i*hauteur*profondeur + (j + 1)*profondeur + k;
					face2.s3 = (i + 1)*hauteur*profondeur + (j + 1)*profondeur + k;
				}
				flip = !flip;
			}
		}
	}
}


voxel::~voxel()
{
}
