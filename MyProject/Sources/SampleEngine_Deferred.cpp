/*
EngineGL overloaded for custom rendering
*/
#include "SampleEngine_Deferred.h"

const std::string ressourceModelPath = "D:/DevLocal/Resources/";
const std::string ressourceTexturePath = "D:/DevLocal/Resources/Textures/";


SampleEngine_Deferred::SampleEngine_Deferred(int width, int height) :
	DeferredEngineGL(width, height)
{
	LOG_INFO << "# - " << __FUNCTION__ << std::endl;

}

SampleEngine_Deferred::~SampleEngine_Deferred()
{}


bool SampleEngine_Deferred::init(std::string filename)
{
	DeferredEngineGL::init(ressourceModelPath + "Scenes/sponza.obj");
	
	LOG_INFO << "initialisation complete" << std::endl;
	return(true);
}


void SampleEngine_Deferred::render()
{
	DeferredEngineGL::render();
}


