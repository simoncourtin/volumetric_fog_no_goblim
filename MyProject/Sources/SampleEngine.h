/*
 *	(c) XLim, UMR-CNRS
 *	Authors: G.Gilet
 *
 */

#ifndef _SAMPLE_ENGINE_H
#define _SAMPLE_ENGINE_H

#include <map>
#include <string>
#include "Engine/OpenGL/EngineGL.h"
#include "Materials/BoundingBoxMaterial/BoundingBoxMaterial.h"

#include "Materials/ColorMaterial/ColorMaterial.h"
#include "Materials/NormalMaterial/NormalMaterial.h"
#include "Materials/DeferredMaterial/DeferredMaterial.h"
#include "Materials/ExpeMaterial/ExpeMaterial.h"
#include "Effects/PostProcess/PostProcess.h"

#include "Materials/Sky/Sky.h"

#include "Effects/Shadow/Shadow.h"
#include "FogMaterial/FogMaterial.h"
#include "VolumetricFog/volumetricFog.h"

class SampleEngine : public EngineGL
{
	public:
		SampleEngine(int width, int height);
		~SampleEngine();

		virtual bool init(std::string filename = "");
		virtual void render();
		virtual void animate(const int elapsedTime);
		
		
		GPUFBO *finalFBO;

	protected:
		int x,y,z;
		vector<Node *> brouillards;
		volumetricFog* fog;
		FogMaterial * f;

};
#endif