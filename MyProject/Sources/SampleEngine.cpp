﻿/*
EngineGL overloaded for custom rendering
*/
#include "SampleEngine.h"
#include "Engine/OpenGL/v4/GLProgram.h"
#include "Engine/OpenGL/SceneLoaderGL.h"
#include "Engine/Base/NodeCollectors/MeshNodeCollector.h"
#include "Engine/Base/NodeCollectors/FCCollector.h"

#include "GPUResources/Textures/GPUTextureCubeMap.h"
#include "GPUResources/Textures/GPUTexture3D.h"

#include "Brouillard\Brouillard.h"
#include "FogMaterial/FogMaterial.h"
#include "VolumetricFog/volumetricFog.h"

#include "GPUResources/GPUInfo.h"
#include "Effects/DepthCopy/DepthCopy.h"
#include "Engine/Base/Frustum.h"
#include "Effects/ComputeShaderTest/ComputeShaderTest.h"

#include "Engine/Base/XMLLoader.h"

#include "Materials/GBuffersMaterial/GBuffersMaterial.h"
#include "GPUResources/Noise/GPUFeatureNoise3.h"
//#include "Noise/GPUFeatureNoise.h"
#include "Effects/DeferredLRPNoise3/DeferredLRPNoise3.h"
#include "GPUResources/Textures/GPUTexture3D.h"
//fichier de configuration pour tout ce qui est chemin, ...
#include "Config.h"

#define M_PI 3.141592653589793238462643383280

float teta = 0.0f;
bool usePostProcess = true;

glm::vec3 backgroundColor;

PostProcess *PP;
GPUFBO *colorFbo, *gbuffers, *bloomFbo, *normalFbo;
//GBuffersNoiseDisplacedMaterial* gbuffer;
GBuffersMaterial* gbuffer;
DeferredLRPNoise3 *defNoise;


Node* focusNode;


SampleEngine::SampleEngine(int width, int height) :
EngineGL (width, height)
{
	LOG_INFO << "# - " << __FUNCTION__ << std::endl;
	
	
}

SampleEngine::~SampleEngine()
{}


bool SampleEngine::init(std::string filename)
{
	//On ajoute une lumière (on peut en ajouter plusieurs)
	LightMaterial* lmat = new LightMaterial ("LightMaterial-" + string ("DefaultLight"), glm::vec4 (1.f));
	addLight(string("WhiteLight"), lmat, glm::vec3(0.f, 10.f, 0.f), POINT_LIGHT, glm::vec4(1.f));

	SceneLoaderGL* sceneloader = new SceneLoaderGL();	
	
	
	/*Node * ville = sceneloader->loadScene(ressourceScenePath + "Ville/Street environment_V01.obj");
	ville->frame()->scale(glm::vec3(0.001));
	ville->frame()->translate(glm::vec3(0.0, -50.0, 0.0));
	scene->getSceneNode()->adopt(ville);*/
	

	
	/*Node * city = sceneloader->loadScene(ressourceModelPath + "City_Block.obj");
	city->frame()->scale(glm::vec3(0.02));
	city->frame()->rotate(glm::vec3(1.0,0.0,0.0),-M_PI/2);
	scene->getSceneNode()->adopt(city);*/
	
	
	Node * mountains = sceneloader->loadScene(ressourceModelPath + "Mountains.obj");
	mountains->frame()->scale(glm::vec3(1));
	mountains->frame()->translate(glm::vec3(-10.0, 0.0, -10.0));
	mountains->setMaterial(new ColorMaterial("material_vert", glm::vec4(0.17, 0.5, 0.0, 1.0)),true);
	scene->getSceneNode()->adopt(mountains);

	Node * castle = sceneloader->loadScene(ressourceScenePath + "Castle/Castle OBJ.obj");
	castle->frame()->scale(glm::vec3(0.01));
	castle->frame()->translate(glm::vec3(0.0, 13.5, 0.0));
	scene->getSceneNode()->adopt(castle);

	Node * farmhouse = sceneloader->loadScene(ressourceScenePath + "Old_farm/Farmhouse OBJ.obj");
	farmhouse->frame()->scale(glm::vec3(0.002));
	farmhouse->frame()->translate(glm::vec3(200.0, 67.0, 50.0));
	scene->getSceneNode()->adopt(farmhouse);
	
	
	//sky box A afficher en tout temps
	Node* skyObject = scene->getNode("MySky");
	skyObject->setModel(scene->m_Models.get<ModelGL>(ressourceObjPath + "Square.obj"));
	skyObject->setMaterial(new Sky("SkyMaterial"));
	skyObject->frame()->scale(glm::vec3(10));
	scene->getSceneNode()->adopt(skyObject);
	
	/*
	Node * ecastle = sceneloader->loadScene(ressourceScenePath + "Eastern_castle/eastern ancient casttle.obj");
	ecastle->frame()->scale(glm::vec3(0.05));
	scene->getSceneNode()->adopt(ecastle);
	*/

	/*
	Node * sponza = sceneloader->loadScene(ressourceScenePath + "Sponza/Objets/sponza.obj");
	sponza->frame()->scale(glm::vec3(0.010));
	scene->getSceneNode()->adopt(sponza);
	*/

	Node* rNode = mountains;
	Node* fog_node = new Node("fog_node");

	focusNode = mountains;


	// Texture 2D Array exemple
	/*
	GPUTexture2DArray *g = scene->getResource<GPUTexture2DArray>("Test");
	g->create(1024,1024,6);
	g->addLayer(ressourceTexPath + "Bunnsy_D.png",0,true,glm::vec4(1.0,0.0,0.0,1.0));
	g->addLayer(ressourceTexPath + "Bunny_D.png",4);
	*/

	// Texture 3D exemple
	/*
	GPUTexture3D *g = scene->getResource<GPUTexture3D>("Test");
	g->create(512,512,512);;
	float* test = new float[512*512*512*4];
	for (int i = 0;i < 512;i++)
		for(int j = 0;j < 512;j++)
			for(int k = 0;k < 512;k++)
			{
				int idx = (i*(512*512) + j*(512)+k);
				test[4*idx] = (float)i/512.0;
				test[4*idx+1] = (float)j/512.0;
				test[4*idx+2] = (float)k/512.0;
				
				//test[4*idx] = 0.2;
				//test[4*idx+1] = 0.2;
				//test[4*idx+2] = 0.8;

				test[4*idx+3] = 1.0;
			}
	g->fillData(test);

	TextureMaterial* te = new TextureMaterial("test",g);
	*/
	
	;

	//rNode->setMaterial(f, true);

	Brouillard * b = new Brouillard("BrouillardTest");
	//Le "true" est tres important dans le set 
	fog_node->setMaterial(b, true);

	scene->getManipulatedNode()->frame()->scale(glm::vec3(10.0));
	fog_node->frame()->translate(glm::vec3(0.0, 0.250, 0.0));
	fog_node->frame()->scale(glm::vec3(20));
	

	//creation du brouillard

	
	x =160, y = 190, z = 128;
	//x = 16, y = 16, z = 16;
	brouillards = vector<Node *>();
	brouillards.push_back(fog_node);

	fog = new volumetricFog(x,y,z);
	
	f = new FogMaterial("Fog", glm::vec3(0,1, 0),0.05);
	f->setDim(glm::vec3(x, y, z));
	fog->preRender(brouillards);
	f->setCamFog(fog->getfog(), x, y, z);

	castle->setMaterial(f, true);
	skyObject->setMaterial(f, true);
	mountains->setMaterial(f, true);
	//adpot des objets
	if (rNode != NULL)
		scene->getSceneNode()->adopt(rNode);
	
	
	finalFBO = scene->getResource<GPUFBO>("Final");
	finalFBO->create(2048, 2048, 1, true, GL_RGBA8, GL_TEXTURE_2D, 1);

	lightingModel = new LightingModelGL("LightingModel", scene->getRoot());
	lightingModel->bind();			// Bind the light buffer for rendering

	glClearColor(1.0, 1.0, 1.0, 1.0);
	//glEnable(GL_BLEND);
	//glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	this->onWindowResize(w_Width, w_Height);

	allNodes->collect (scene->getRoot ());
	renderedNodes->collect (scene->getRoot ());

	for (unsigned int i = 0; i < allNodes->nodes.size (); i++)
		allNodes->nodes[i]->animate (0);


	Scene::getInstance()->camera()->updateBuffer();
	LOG_INFO << "initialisation complete" << std::endl;
	return(true);
}


void SampleEngine::render ()
{

	finalFBO->enable();
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		
	for (unsigned int i = 0; i < renderedNodes->nodes.size(); i++)
		renderedNodes->nodes[i]->render();
	finalFBO->disable();

	finalFBO->display(glm::vec4(0.0, 0.0, 1.0, 1.0), 0);
	drawBBAndLight();
}

void SampleEngine::animate (const int elapsedTime)
{
	// Animate each node
	for (unsigned int i = 0; i < allNodes->nodes.size (); i++)
		allNodes->nodes[i]->animate (elapsedTime);
	// force update of lighting model
	lightingModel->update (true);
	Scene::getInstance()->camera()->updateBuffer();

	// update du fog
	//fog->preRender(brouillards);
	//f->setCamFog(fog->getfog(), x, y, z);
	
}

