#pragma once
#include "Engine/Base/Node.h"
#include "Engine/Base/Scene.h"

class volumetricFog
{
public:
	volumetricFog(int x, int y, int z);
	void preRender(std::vector<Node*> fogs);
	glm::vec4*** getfog() {
		return fogAbsorbance;
	}
	~volumetricFog();
private :
	int x, y, z;
	glm::vec4*** fogAbsorbance;
};

