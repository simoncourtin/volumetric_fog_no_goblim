#version 430

#extension GL_ARB_shading_language_include : enable
#include "/Materials/Common/Common"
#line 6

layout(std140) uniform CPU
{
	mat4 Model;
vec3 pos;
};

out gl_PerVertex{
	vec4 gl_Position;
float gl_PointSize;
float gl_ClipDistance[];
};

layout(location = 0) in vec3 Position;
layout(location = 2) in vec3 Normal;
layout(location = 3) in vec3 TexCoord;

out vec3 v_fpos;
out vec3 v_texCoord;
out vec3 v_Normal;
out vec3 v_modelPos;

void main() {

	gl_Position = ViewProj * Model * vec4(Position, 1.0);
	v_fpos = (Model * vec4(Position, 1.0)).xyz;
	v_Normal = (Model * vec4(Normal, 0.0)).xyz;
	v_texCoord = TexCoord;
	v_modelPos = Position;
}
