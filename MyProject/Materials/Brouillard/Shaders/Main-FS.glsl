#version 450 core

#extension GL_NV_bindless_texture : require

#extension GL_ARB_shading_language_include : enable
#include "/Materials/Common/Lighting/Lighting"
#line 3 


layout(std140) uniform CPU
{
	sampler3D colorTex;

};


in vec3 v_fpos;
in vec3 v_texCoord;
in vec3 v_Normal;
in vec3 v_modelPos;


layout(location = 0) out vec4 Color;

void main()
{
	//vec4 color = texture(colorTex,vec3(v_texCoord.xy,level));
	vec4 color = texture(colorTex, vec3(abs(v_fpos)));

	Color = addPhong(v_fpos, normalize(v_Normal), color, color, vec4(1.0), vec4(0.4, 0.8, 0.4, 64.0));

}