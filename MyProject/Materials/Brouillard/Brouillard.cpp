#include "Brouillard.h"
#include "Engine/Base/Node.h"
#include "Engine/Base/Scene.h"


Brouillard::Brouillard(std::string name) :
	MaterialGL(name, "Brouillard")
{
	model = vp->uniforms()->getGPUmat4("Model"); 	
	colorSampler = fp->uniforms()->getGPUsampler("colorTex");
	pos = vp->uniforms()->getGPUvec3("pos");
	setPos(glm::vec3(0.0));

	texture3d = new GPUTexture3D("brouillard3DTexture", width, height, depth);

	remplire();
}

void Brouillard::remplire() {
	//dimensions de la text3D
	height = 512;
	width = 512;
	depth = 512;

	//canaux couleur de la  	
	int nb_canaux = 4;

	//Tableau test de texture 3D
	//On itere sur chaque dimmention
	float* texture_data = new float[width * height * depth * nb_canaux];
	for (int i = 0; i < width; i++)
		for (int j = 0; j < height; j++)
			for (int k = 0; k < depth; k++)
			{
				int idx = ( i * height * depth + j * depth + k);

				//coefficient d'absorbtion par cannal 
				texture_data[nb_canaux * idx] = 0.05;
				texture_data[nb_canaux * idx + 1] = 0.05;
				texture_data[nb_canaux * idx + 2] = 0.05;
				texture_data[nb_canaux * idx + 3] = 0.05;
			}

	//on passe les donn�es � la texture 3D
	texture3d->fillData(texture_data);

	//sampler vers les shaders 	
	colorSampler->Set(texture3d->getHandle());
}

Brouillard::~Brouillard()
{
}

void Brouillard::setPos(glm::vec3 & pos)
{
	this->pos->Set(pos);
}

void Brouillard::render(Node *o)
{
	this->setPos(Scene::getInstance()->camera()->convertPtTo(glm::vec3(0.0f), o->frame()));
		
	if (m_ProgramPipeline)
	{	
		m_ProgramPipeline->bind();
		o->drawGeometry(GL_TRIANGLES);
		m_ProgramPipeline->release();
	}
	
}

void Brouillard::update(Node* o, const int elapsedTime)
{
	if (o->frame()->updateNeeded())
	{
		//chaque frame on passe la matrice Model
		//A enlever par la suite pour le brouillard
		model->Set(o->frame()->getRootMatrix());
	}
}

glm::vec4 Brouillard::getOpacity(glm::vec4 pos)
{
	//std::cout << pos.x << " " << pos.y << " " << pos.z << std::endl;
	/**TODO :
		return the actual fog color at the point pos
	*/
	//si on est en dehors du brouillard retourne un vecteur null
	if (pos.x < 0 || pos.y < 0 || pos.z < 0 || pos.x >1 || pos.y >1 || pos.z >1) {
		return glm::vec4(0,0,0,1);
	}
	int x = pos.x * width;
	int y = pos.y * height;
	int z = pos.z * depth;

	int indice = x + y* width + z * height* width;

	return texture3d->getColor(indice);
}
