#ifndef _BROUILLARD_H
#define _BROUILLARD_H


#include "Engine/OpenGL/MaterialGL.h"
#include "Engine/OpenGL/Lighting/LightingModelGL.h"
#include <memory.h>
#include "GPUResources/Textures/GPUTexture3D.h"

class Brouillard : public MaterialGL
{
public:
	Brouillard(std::string name);
	void remplire();
	~Brouillard();

	void setPos(glm::vec3 & pos);

	virtual void render(Node *o);
	virtual void update(Node* o, const int elapsedTime);

	glm::vec4 getOpacity(glm::vec4 pos);

private:
	int width, height, depth;
	GPUmat4* model;
	GPUvec3* pos;
	GPUvec3 * pos_min;
	GPUvec3 * pos_max;

	GPUTexture3D * texture3d;
	GPUsampler * colorSampler;

};

#endif