#version 430
#extension GL_ARB_shading_language_include : enable
#extension GL_ARB_bindless_texture : enable

layout(std140) uniform CPU
{	
	vec4 CPU_color;
	sampler2D tex;
};

layout (location = 0) out vec4 Color;
in vec2 texCoord;

void main()
{
	Color = texture(tex, texCoord);
}