#version 430

#extension GL_ARB_shading_language_include : enable
#include "/Materials/Common/Common"
#line 6 

layout(std140) uniform CPU
{
	mat4 MVP;
	vec3 pos;
};

 out gl_PerVertex {
        vec4 gl_Position;
        float gl_PointSize;
        float gl_ClipDistance[];
    };
    
layout (location = 0) in vec3 Position;
out vec2 vTexCoord;
layout (location = 3) in vec2 texCoord;

void main() {

	gl_Position = MVP * vec4(Position + pos, 1.0);
	vTexCoord = texCoord;
}
