#include "Sky.h"
#include "Engine/Base/Node.h"
#include "Engine/Base/Scene.h"


Sky::Sky(std::string name) :
	MaterialGL(name, "Sky")
{
	modelViewProj = vp->uniforms()->getGPUmat4("MVP");
	pos = vp->uniforms()->getGPUvec3("pos");

	tex = new GPUTexture2D("C:/Home/Output/MyProject/Debug/Textures/skybox.jpg");
	tex->setUpSampler(GL_CLAMP_TO_BORDER_ARB, GL_NEAREST, GL_NEAREST);

	samp = fp->uniforms()->getGPUsampler("tex");
	samp->Set(tex->getHandle());
}

Sky::~Sky()
{
}

void Sky::setPos(glm::vec3 & pos)
{
	this->pos->Set(pos);
}

void Sky::render(Node *o)
{
	this->setPos(Scene::getInstance()->camera()->convertPtTo(glm::vec3(0.0f), o->frame()));
	if (m_ProgramPipeline)
	{
		m_ProgramPipeline->bind();
		o->drawGeometry(GL_TRIANGLES);
		m_ProgramPipeline->release();
	}
}

void Sky::update(Node* o, const int elapsedTime)
{
	modelViewProj->Set(o->frame()->getTransformMatrix());
}