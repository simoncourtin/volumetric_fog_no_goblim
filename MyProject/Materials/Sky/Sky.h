#ifndef _SKY_H
#define _SKY_H


#include "Engine/OpenGL/MaterialGL.h"
#include "Engine/OpenGL/Lighting/LightingModelGL.h"
#include <memory.h>

class Sky : public MaterialGL
{
public:
	Sky(std::string name);
	~Sky();

	void setPos(glm::vec3 & pos);

	virtual void render(Node *o);
	virtual void update(Node* o, const int elapsedTime);


private:
	GPUmat4* modelViewProj;

	GPUTexture2D* tex;
	GPUsampler* samp;

	GPUvec3* pos;

};

#endif