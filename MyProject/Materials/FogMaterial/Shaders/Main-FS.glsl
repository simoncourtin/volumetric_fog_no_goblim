#version 450 core

#extension GL_NV_bindless_texture : require

#extension GL_ARB_shading_language_include : enable
#line 8 


layout(std140) uniform CPU
{
	mat4 NormalMV;
	vec3 globalColor;
	vec3 lightPos;
	vec3 campos;
	vec3 fogDim;
	float coef_diffus;
	sampler3D colorTex;
	bool active_absorption;
	bool active_diffusion;

};

in vec3 texCoord;
in vec3 camDir;
in mat3 TSpace;
in vec3 fpos;
in vec3 normal;

in vec3 lDir;
in vec3 cDir;

in vec3 v_Normal;
in vec4 v_Tangent;

in vec4 fpos2;
in vec3 fcamPos;
in vec3 flightColor;

layout(location = 0) out vec4 Color;
const float PI = 3.1415926535897932384626433832795;
const int largeur = textureSize(colorTex, 0).x;
const int hauteur = textureSize(colorTex, 0).y;
const int profondeur = textureSize(colorTex, 0).z;
const int size = largeur*hauteur*profondeur;
float HenyeyGreenstein(float theta, float g);



float HenyeyGreenstein(float theta, float g) {
	return  1 - clamp((1 / (4 * PI)) * ((1 - g*g) / pow(1 + g*g - 2 * g * cos(theta), 3 / 2)),0,1);
}


vec4 bresenham(float x1, float y1, float z1, const float x2, const float y2, const float z2);



vec4 bresenham(float x1, float y1, float z1, const float x2, const float y2, const float z2)
{
	float i, dx, dy, dz, l, m, n, x_inc, y_inc, z_inc, err_1, err_2, dx2, dy2, dz2;
	vec3 pos;
	
	float theta = 0.0;

	//dimension calcul dynamic
	float pasX = 1 / fogDim.x;
	float pasY = 1 / fogDim.y;
	float pasZ = 1 / fogDim.z;

	x1 = x1 - mod(x1, pasX);
	y1 = y1 - mod(y1, pasY);
	z1 = z1 - mod(z1, pasZ);

	vec4 absorbance;
	vec4 color = vec4(globalColor, 1.0);
	//color = color * dot(camDir,normal);
	pos.x = x1;
	pos.y = y1;
	pos.z = z1;

	dx = x2 - x1;
	dy = y2 - y1;
	dz = z2 - z1;

	if (dx < 0)
		x_inc = - pasX;
	else
		x_inc = pasX;
	l = abs(dx);
	if (dy < 0)
		y_inc = -pasY;
	else
		y_inc = pasY;
	m = abs(dy);
	if (dz < 0)
		z_inc = -pasZ;
	else
		z_inc = pasZ;
	n = abs(dz);

	dx2 = l * 2;
	dy2 = m * 2;
	dz2 = n * 2;

	if ((l >= m) && (l >= n)) {
		err_1 = dy2 - l;
		err_2 = dz2 - l;
		for (i = 0; i < l; i++) {
			//absorbance
			absorbance = texture(colorTex, vec3(pos.x, pos.y, pos.z));
			if(active_absorption)
				color = vec4(color.x * HenyeyGreenstein(theta, absorbance.x), color.y * HenyeyGreenstein(theta, absorbance.y), color.z * HenyeyGreenstein(theta, absorbance.z), color.w);
			//Application de la lumiere pour la diffusion 
			vec3 light_voxel = normalize(pos.xyz - lightPos);
			vec3 voxel_cam = normalize(campos - pos.xyz);
			
			float theta_light = dot(light_voxel, voxel_cam);

			vec4 color_diffus = vec4(flightColor.x * HenyeyGreenstein(theta_light, absorbance.x), flightColor.y * HenyeyGreenstein(theta_light, absorbance.y), flightColor.z * HenyeyGreenstein(theta_light, absorbance.z), 1);
			if (active_diffusion)
				color += color_diffus;// *coef_diffus;

			
			if (err_1 > 0) {
				pos.y += y_inc;
				err_1 -= dx2;
			}
			if (err_2 > 0) {
				pos.z += z_inc;
				err_2 -= dx2;
			}
			err_1 += dy2;
			err_2 += dz2;
			pos.x += x_inc;
		}
	}
	else if ((m >= l) && (m >= n)) {
		err_1 = dx2 - m;
		err_2 = dz2 - m;
		for (i = 0; i < m; i++) {
			//absorbance
			absorbance = texture(colorTex, vec3(pos.x, pos.y, pos.z));
			if (active_absorption)
				color = vec4(color.x * HenyeyGreenstein(theta, absorbance.x), color.y * HenyeyGreenstein(theta, absorbance.y), color.z * HenyeyGreenstein(theta, absorbance.z), color.w);
			//Application de la lumiere pour la diffusion 
			vec3 light_voxel = normalize(pos.xyz - lightPos);
			vec3 voxel_cam = normalize(campos - pos.xyz);

			float theta_light = dot(light_voxel, voxel_cam);

			vec4 color_diffus = vec4(flightColor.x * HenyeyGreenstein(theta_light, absorbance.x), flightColor.y * HenyeyGreenstein(theta_light, absorbance.y), flightColor.z * HenyeyGreenstein(theta_light, absorbance.z), 1);
			if (active_diffusion)
				color += color_diffus;// * coef_diffus; 
			
			if (err_1 > 0) {
				pos.x += x_inc;
				err_1 -= dy2;
			}
			if (err_2 > 0) {
				pos.z += z_inc;
				err_2 -= dy2;
			}
			err_1 += dx2;
			err_2 += dz2;
			pos.y += y_inc;
		}
	}
	else {
		err_1 = dy2 - n;
		err_2 = dx2 - n;
		for (i = 0; i < n; i++) {
			//absorbance
			absorbance = texture(colorTex, vec3(pos.x, pos.y, pos.z));
			if (active_absorption)
				color = vec4(color.x * HenyeyGreenstein(theta, absorbance.x), color.y * HenyeyGreenstein(theta, absorbance.y), color.z * HenyeyGreenstein(theta, absorbance.z), color.w);
			//Application de la lumiere pour la diffusion 
			vec3 light_voxel = normalize(pos.xyz - lightPos);
			vec3 voxel_cam = normalize(campos - pos.xyz);

			float theta_light = dot(light_voxel, voxel_cam);
			vec4 color_diffus = vec4(flightColor.x * HenyeyGreenstein(theta_light, absorbance.x), flightColor.y * HenyeyGreenstein(theta_light, absorbance.y), flightColor.z * HenyeyGreenstein(theta_light, absorbance.z), 1);
			if (active_diffusion)
				color += color_diffus;// * coef_diffus; 
			
			if (err_1 > 0) {
				pos.y += y_inc;
				err_1 -= dz2;
			}
			if (err_2 > 0) {
				pos.x += x_inc;
				err_2 -= dz2;
			}
			err_1 += dy2;
			err_2 += dx2;
			pos.z += z_inc;
		}
	}
	//absorbance
	absorbance = texture(colorTex, vec3(pos.x, pos.y, pos.z));
	if (active_absorption)
		color = vec4(color.x * HenyeyGreenstein(theta, absorbance.x), color.y * HenyeyGreenstein(theta, absorbance.y), color.z * HenyeyGreenstein(theta, absorbance.z), color.w);
	//Application de la lumiere pour la diffusion 
	vec3 light_voxel = normalize(pos.xyz - lightPos);
	vec3 voxel_cam = normalize(campos - pos.xyz);

	float theta_light = dot(light_voxel, voxel_cam);
	vec4 color_diffus = vec4(flightColor.x * HenyeyGreenstein(theta_light, absorbance.x), flightColor.y * HenyeyGreenstein(theta_light, absorbance.y), flightColor.z * HenyeyGreenstein(theta_light, absorbance.z), 1);
	
	if (active_diffusion)
		color += color_diffus;// * coef_diffus;
	return color;
}

void main()
{
	Color = bresenham(fpos2.x, fpos2.y, fpos2.z,0, 0, 0 );
}



