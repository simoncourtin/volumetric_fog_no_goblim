#version 430

#extension GL_ARB_shading_language_include : enable
#include "/Materials/Common/Common"
#line 6

layout(std140) uniform CPU
{
	mat4 MVP;
	mat4 MV;
	vec3 lightColor;
	vec3 lightPos;
	vec3 camPos;
};

out gl_PerVertex{
	vec4 gl_Position;
	float gl_PointSize;
	float gl_ClipDistance[];
};

layout(location = 0) in vec3 Position;
layout(location = 2) in vec3 Normal;
layout(location = 3) in vec3 TexCoord;
layout(location = 4) in vec4 Tangent;

out vec3 texCoord;
out vec3 fpos;
out vec3 camDir;
out mat3 TSpace;
out vec3 normal;

out vec3 v_Normal;
out vec4 v_Tangent;

out vec3 lDir;
out vec3 cDir;

out vec4 fpos2;
out vec3 fcamPos;
out vec3 flightColor;

void main()
{
	gl_Position = MVP * vec4(Position, 1.0);

	fpos2 = gl_Position;
	fpos = (MV * vec4(Position, 1.0)).xyz;
	normal = (MV * vec4(Normal, 0.0)).xyz;
	texCoord = TexCoord;

	vec3 B = normalize(Tangent.w*(cross(Normal, Tangent.xyz)));
	TSpace = transpose(mat3(Tangent.xyz, B, Normal));

	v_Normal = Normal;
	v_Tangent = Tangent;

	camDir = normalize(camPos - fpos);
	fcamPos = (MV * vec4(Position, 1.0)).xyz;
	lDir = lightPos - Position;
	flightColor = lightColor;

	cDir = TSpace * normalize(camPos - Position);
	lDir = TSpace * normalize(lDir);


}
