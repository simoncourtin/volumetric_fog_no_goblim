#pragma once
#include "Engine/OpenGL/MaterialGL.h"
#include "Engine/OpenGL/Lighting/LightingModelGL.h"
#include "GPUResources/Textures/GPUTexture3D.h"

class FogMaterial : public MaterialGL
{
public:
	FogMaterial(std::string name, glm::vec3 color, float coef_diffusion);
	void setDim(glm::vec3 dim);
	~FogMaterial();

	virtual void render(Node *o);

	void setCamFog(glm::vec4*** fogAbsorbance, int width, int height, int depth);

	GPUmat4 *modelViewProj, *modelView, *modelViewF;

	GPUTexture3D * camFog;
	GPUsampler * colorSampler;
	GPUfloat * coef_diffus;

	GPUvec3* camPos;
	GPUvec3* lightPos;
	GPUvec3* lightColor;
	GPUvec3* color;
	GPUvec3* dim;
	GPUbool * active_absorption;
	GPUbool * active_diffusion;

	glm::vec3 light_color;
	float light_intensity;
	float abs_intensity;
	bool b_absorption;
	bool b_diffusion;

	virtual void displayInterface(const char* title);

protected:
	bool loadShaders();
};

