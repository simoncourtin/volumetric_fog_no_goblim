#include "Engine/Base/Node.h"
#include "Engine/Base/Scene.h"
#include "FogMaterial.h"
FogMaterial::FogMaterial(std::string name, glm::vec3 color, float coef_diffusion):MaterialGL(name, "FogMaterial")
{
	modelViewProj = vp->uniforms()->getGPUmat4("MVP");
	modelView = vp->uniforms()->getGPUmat4("MV");

	camPos = fp->uniforms()->getGPUvec3("campos");
	camPos = vp->uniforms()->getGPUvec3("camPos");
	lightColor = vp->uniforms()->getGPUvec3("lightColor");
	lightPos = fp->uniforms()->getGPUvec3("lightPos");
	colorSampler = fp->uniforms()->getGPUsampler("colorTex");
	coef_diffus = fp->uniforms()->getGPUfloat("coef_diffus");

	modelViewF = fp->uniforms()->getGPUmat4("NormalMV");

	dim = fp->uniforms()->getGPUvec3("fogDim");

	active_absorption = fp->uniforms()->getGPUbool("active_absorption");
	active_diffusion = fp->uniforms()->getGPUbool("active_diffusion");

	this->color = fp->uniforms()->getGPUvec3("globalColor");
	this->color->Set(color);
	this->coef_diffus->Set(coef_diffusion);

	light_color = glm::vec3(0.1, 0.1, 0.1);
	light_intensity = 0.5;
	lightColor->Set(light_color * light_intensity);
}

void FogMaterial::setDim(glm::vec3 dim) {
	this->dim->Set(dim);
}

void FogMaterial::render(Node *o)
{
	camPos->Set(Scene::getInstance()->camera()->convertPtTo(glm::vec3(0.0, 0.0, 0.0), Scene::getInstance()->frame()));
	lightPos->Set(Scene::getInstance()->getNode("Light")->frame()->convertPtTo(glm::vec3(0.0, 0.0, 0.0), o->frame()));

	lightColor->Set(light_color * light_intensity);
	active_absorption->Set(b_absorption);
	active_diffusion->Set(b_diffusion);

	if (m_ProgramPipeline)
	{

		m_ProgramPipeline->bind();

		modelViewProj->Set(o->frame()->getTransformMatrix());
		modelView->Set(o->frame()->getRootMatrix());
		modelViewF->Set(glm::transpose(glm::inverse(o->frame()->getRootMatrix())));

		o->drawGeometry(GL_TRIANGLES);
		m_ProgramPipeline->release();

	}
}

void FogMaterial::setCamFog(glm::vec4 *** fogAbsorbance , int width, int height, int depth)
{
	camFog = new GPUTexture3D("brouillard3DTexture", width, height, depth);

	int nbCanaux = 4;
	//Tableau test de texture 3D
	//On itere sur chaque dimmention
	float* text_value = new float[width * height * depth * nbCanaux];
	for (int i = 0; i < width; i++)
		for (int j = 0; j < height; j++)
			for (int k = 0; k < depth; k++)
			{
				int idx = (i * height * depth + j * depth + k);

				//la couleur depends de la position
				text_value[nbCanaux * idx] = fogAbsorbance[i][j][k].r;
				text_value[nbCanaux * idx + 1] = fogAbsorbance[i][j][k].g;
				text_value[nbCanaux * idx + 2] = fogAbsorbance[i][j][k].b;

				text_value[nbCanaux * idx + 3] = fogAbsorbance[i][j][k].a ;
			}

	//on passe les donn�es � la texture 3D
	camFog->fillData(text_value);

	//sampler vers les shaders 	
	colorSampler->Set(camFog->getHandle());

}

void FogMaterial::displayInterface(const char* title) {
	ImGui::Text(title);
	ImGui::SliderFloat("Intensite de la lumiere", &light_intensity, 0.0f, 1.0f, "%.3f");
	static ImVec4 color = ImColor(light_color.r, light_color.g, light_color.b, 1.0f);
	ImGui::Text("Light Color :");
	ImGui::SameLine();
	if (ImGui::ColorButton(color))
		ImGui::OpenPopup("Light Color");
	if (ImGui::BeginPopup("Light Color"))
	{
		ImGui::ColorPicker4("", (float*)&color);
		light_color = glm::vec3(color.x, color.y, color.z);
		ImGui::EndPopup();
	}

	ImGui::Text("Parametres brouillard:");
	ImGui::Checkbox(" : Absorption ON/OFF", &b_absorption);
	ImGui::Checkbox(" : Diffusion ON/OFF", &b_diffusion);
}
