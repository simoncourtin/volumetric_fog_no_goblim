#pragma once
#include <iostream>
#include "Engine/Base/GeometricModel.h"
#include "Engine/Base/GeometricModelLoader.h"
#include "Engine/Base/Scene.h"
class voxel
{
public:
	std::vector < glm::vec3 > listVertex;
	std::vector <Face> listFaces;

	voxel(int largeur, int hauteur, int profondeur);


	~voxel();
};

